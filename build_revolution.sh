#!/bin/bash

pushd matrix-js-sdk
yarn install
yarn build
yarn link
popd

pushd matrix-react-sdk
yarn link matrix-js-sdk
yarn install
yarn build
yarn link
popd

pushd riot-web
yarn link matrix-js-sdk
yarn link matrix-react-sdk
yarn install
yarn build
popd

pushd riot-desktop
yarn install
ln -s ../riot-web/webapp .
yarn run build:native
yarn build -l dir
popd
